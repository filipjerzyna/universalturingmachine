﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTuringsMachine
{
    static class Converter
    {
        static string endPart = "110";
        static string replacement = "10";

        public static string ToTuringBinaryString(long number)
        {
            string binaryString = Convert.ToString(number, 2);
            binaryString = binaryString.Replace("1", replacement);
            return binaryString + endPart;
        }

        public static long ToLong(string turingBinaryInput)
        {
            string withoutTrailingZeros = turingBinaryInput.TrimEnd(new[] { '0' });
            string inputWithoutEndPart = withoutTrailingZeros.Substring(0, withoutTrailingZeros.Length - 2);
            string binaryString = inputWithoutEndPart.Replace(replacement, "1");

            long number = Convert.ToInt64(binaryString, 2);
            return number;
        }
    }
}
