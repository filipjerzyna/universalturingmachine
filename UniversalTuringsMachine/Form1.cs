﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UniversalTuringsMachine
{
    public partial class Form1 : Form
    {
        private TuringMachine machine;
        public Form1()
        {
            InitializeComponent();
        }

        private void btOk_Click(object sender, EventArgs e)
        {
            try
            {
                long inputNumber = Convert.ToInt64(rtbInput.Text);
                string input = Converter.ToTuringBinaryString(inputNumber);
                rtbTuringBinaryStringInput.Text = input;

                string resultString = machine.ProcessProgram(input);
                long result = Converter.ToLong(resultString);
                rtbOutput.Text = result.ToString();
                rtbTuringBinaryStringOutput.Text = machine.ProcessProgram(input);
            }
            catch { }
        }

        private void btLoad_Click(object sender, EventArgs e)
        {
            machine = new TuringMachine();
            machine.LoadProgram(rtbProgram.Text);

            //throws Exception !
            //lblMachineNumber.Text = "Machine number is: " + Convert.ToString(Convert.ToInt64(rtbProgram.Text.Substring(0, rtbProgram.Text.Length-1), 2));
            //lblMachineNumber.Visible = true;
            MessageBox.Show("Program successfully loaded");
        }

    }
}
