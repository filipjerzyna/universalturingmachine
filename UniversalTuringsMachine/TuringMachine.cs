﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTuringsMachine
{

    class TuringMachine
    {
        //public long Number { get; set; }
        private long state = 0;
        public string BinaryNumber { get; set; }
        private Dictionary<string, string> substitutions = new Dictionary<string, string>();
        private Dictionary<Tuple<long, int>, Tuple<long, int, string>> transitions;
        public TuringMachine()
        {
            substitutions.Add("0", "0");
            substitutions.Add("10", "1");
            substitutions.Add("110", "R");
            substitutions.Add("1110", "L");
            //S instead of Stop for further convenience
            substitutions.Add("11110", "S");
            //CalculateNumber(program);
        }

        public string ProcessProgram(string input)
        {
            state = 0;
            //Reading device position
            int counter = 0;
            var inputArray = input.ToArray();
            Boolean stop = false;

            while (!stop)
            {
                var key = Tuple.Create<long, int>(state, Convert.ToInt32(Convert.ToString(inputArray[counter])));
                var decision = transitions[key];
                state = decision.Item1;
                inputArray[counter] = Convert.ToChar(Convert.ToString(decision.Item2));

                switch (decision.Item3)
                {
                    case "R":
                        counter++;
                        break;
                    case "L":
                        counter--;
                        break;
                    case "S":
                        stop = true;
                        break;
                    default:
                        break;
                }
            }
            return new string(inputArray);
        }

        public void LoadProgram(string program)
        {
            //translating to readable form
            string temp = "";
            string value = "";
            string readableProgram = "";
            for (int i = 0; i < program.Length; i++)
            {
                temp += program[i];

                if (substitutions.TryGetValue(temp, out value))
                {
                    readableProgram += value;
                    temp = "";
                }
            }
            temp = "";
            //decoding
            transitions = new Dictionary<Tuple<long, int>, Tuple<long, int, string>>();

            for (int i = 0; i < readableProgram.Length; i++)
            {
                temp += readableProgram[i];
                if (readableProgram[i] == 'R' || readableProgram[i] == 'L' || readableProgram[i] == 'S')
                {
                    //generating key
                    string token = "";
                    long counter = transitions.Count;
                    if (counter == 0)
                    {
                        token = "00";
                    }
                    else if (counter == 1)
                    {
                        token = "01";
                    }
                    else
                    {
                        token = Convert.ToString(counter, 2);
                    }

                    var key = Tuple.Create(Convert.ToInt64(token.Substring(0, token.Length - 1)), Convert.ToInt32(Convert.ToString(token.ElementAt(token.Length - 1))));

                    var transition = Tuple.Create(Convert.ToInt64(temp.Substring(0, temp.Length - 2)), Convert.ToInt32(Convert.ToString(temp[temp.Length - 2])), Convert.ToString(temp[temp.Length - 1]));
                    transitions.Add(key, transition);

                    temp = "";
                }
            }

        }


        /*
        private void CalculateNumber(string program)
        {
            var list = new List<string>();

            for (int i = 0; i < program.Length; i++)
            {
                list.Add(Convert.ToString(program[i]));
            }


            string temp = "";
            string value = "";
            var elementsToReplace = new List<int>();
            for (int i = 0; i < list.Count; i++)
            {
                temp += list[i];

                if (substitutions.TryGetValue(temp, out value))
                {
                    list[i] = value;
                    temp = "";
                }
                else
                {
                    elementsToReplace.Add(i);
                }
            }

            elementsToReplace.Sort();
            elementsToReplace.Reverse();
            elementsToReplace.ForEach(g => list.RemoveAt(g));
            var output = "";
            list.ForEach(g => output += g);
            Number = Convert.ToInt64(output, 2);
            BinaryNumber = output;
        }
        */

    }
}
