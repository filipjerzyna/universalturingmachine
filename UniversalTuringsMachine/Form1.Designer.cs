﻿namespace UniversalTuringsMachine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtbProgram = new System.Windows.Forms.RichTextBox();
            this.lblProgram = new System.Windows.Forms.Label();
            this.btOk = new System.Windows.Forms.Button();
            this.lblMachineNumber = new System.Windows.Forms.Label();
            this.rtbInput = new System.Windows.Forms.RichTextBox();
            this.btLoad = new System.Windows.Forms.Button();
            this.lblInput = new System.Windows.Forms.Label();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.lblOutput = new System.Windows.Forms.Label();
            this.rtbTuringBinaryStringInput = new System.Windows.Forms.RichTextBox();
            this.rtbTuringBinaryStringOutput = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // rtbProgram
            // 
            this.rtbProgram.Location = new System.Drawing.Point(39, 41);
            this.rtbProgram.Margin = new System.Windows.Forms.Padding(2);
            this.rtbProgram.Name = "rtbProgram";
            this.rtbProgram.Size = new System.Drawing.Size(302, 119);
            this.rtbProgram.TabIndex = 0;
            this.rtbProgram.Text = "001101010110001101001011010100111010010110010111101000011101001010111010001011101" +
    "010001101001011000110101010101101010101101010100110\n";
            // 
            // lblProgram
            // 
            this.lblProgram.AutoSize = true;
            this.lblProgram.Location = new System.Drawing.Point(37, 24);
            this.lblProgram.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProgram.Name = "lblProgram";
            this.lblProgram.Size = new System.Drawing.Size(46, 13);
            this.lblProgram.TabIndex = 1;
            this.lblProgram.Text = "Program";
            // 
            // btOk
            // 
            this.btOk.Location = new System.Drawing.Point(39, 265);
            this.btOk.Margin = new System.Windows.Forms.Padding(2);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(72, 32);
            this.btOk.TabIndex = 2;
            this.btOk.Text = "Process";
            this.btOk.UseVisualStyleBackColor = true;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // lblMachineNumber
            // 
            this.lblMachineNumber.AutoSize = true;
            this.lblMachineNumber.Location = new System.Drawing.Point(155, 168);
            this.lblMachineNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMachineNumber.Name = "lblMachineNumber";
            this.lblMachineNumber.Size = new System.Drawing.Size(0, 13);
            this.lblMachineNumber.TabIndex = 3;
            this.lblMachineNumber.Visible = false;
            // 
            // rtbInput
            // 
            this.rtbInput.Location = new System.Drawing.Point(39, 229);
            this.rtbInput.Margin = new System.Windows.Forms.Padding(2);
            this.rtbInput.Name = "rtbInput";
            this.rtbInput.Size = new System.Drawing.Size(72, 32);
            this.rtbInput.TabIndex = 4;
            this.rtbInput.Text = "167";
            // 
            // btLoad
            // 
            this.btLoad.Location = new System.Drawing.Point(39, 168);
            this.btLoad.Margin = new System.Windows.Forms.Padding(2);
            this.btLoad.Name = "btLoad";
            this.btLoad.Size = new System.Drawing.Size(80, 33);
            this.btLoad.TabIndex = 5;
            this.btLoad.Text = "Load";
            this.btLoad.UseVisualStyleBackColor = true;
            this.btLoad.Click += new System.EventHandler(this.btLoad_Click);
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Location = new System.Drawing.Point(37, 213);
            this.lblInput.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(31, 13);
            this.lblInput.TabIndex = 8;
            this.lblInput.Text = "Input";
            // 
            // rtbOutput
            // 
            this.rtbOutput.Location = new System.Drawing.Point(39, 338);
            this.rtbOutput.Margin = new System.Windows.Forms.Padding(2);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(72, 31);
            this.rtbOutput.TabIndex = 9;
            this.rtbOutput.Text = "";
            // 
            // lblOutput
            // 
            this.lblOutput.AutoSize = true;
            this.lblOutput.Location = new System.Drawing.Point(37, 314);
            this.lblOutput.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblOutput.Name = "lblOutput";
            this.lblOutput.Size = new System.Drawing.Size(39, 13);
            this.lblOutput.TabIndex = 10;
            this.lblOutput.Text = "Output";
            // 
            // rtbTuringBinaryStringInput
            // 
            this.rtbTuringBinaryStringInput.Location = new System.Drawing.Point(115, 229);
            this.rtbTuringBinaryStringInput.Margin = new System.Windows.Forms.Padding(2);
            this.rtbTuringBinaryStringInput.Name = "rtbTuringBinaryStringInput";
            this.rtbTuringBinaryStringInput.ReadOnly = true;
            this.rtbTuringBinaryStringInput.Size = new System.Drawing.Size(229, 32);
            this.rtbTuringBinaryStringInput.TabIndex = 11;
            this.rtbTuringBinaryStringInput.Text = "";
            // 
            // rtbTuringBinaryStringOutput
            // 
            this.rtbTuringBinaryStringOutput.Location = new System.Drawing.Point(115, 338);
            this.rtbTuringBinaryStringOutput.Margin = new System.Windows.Forms.Padding(2);
            this.rtbTuringBinaryStringOutput.Name = "rtbTuringBinaryStringOutput";
            this.rtbTuringBinaryStringOutput.ReadOnly = true;
            this.rtbTuringBinaryStringOutput.Size = new System.Drawing.Size(226, 31);
            this.rtbTuringBinaryStringOutput.TabIndex = 12;
            this.rtbTuringBinaryStringOutput.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 412);
            this.Controls.Add(this.rtbTuringBinaryStringOutput);
            this.Controls.Add(this.rtbTuringBinaryStringInput);
            this.Controls.Add(this.lblOutput);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.lblInput);
            this.Controls.Add(this.btLoad);
            this.Controls.Add(this.rtbInput);
            this.Controls.Add(this.lblMachineNumber);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.lblProgram);
            this.Controls.Add(this.rtbProgram);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Universal Turing Machine";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtbProgram;
        private System.Windows.Forms.Label lblProgram;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.Label lblMachineNumber;
        private System.Windows.Forms.RichTextBox rtbInput;
        private System.Windows.Forms.Button btLoad;
        private System.Windows.Forms.Label lblInput;
        private System.Windows.Forms.RichTextBox rtbOutput;
        private System.Windows.Forms.Label lblOutput;
        private System.Windows.Forms.RichTextBox rtbTuringBinaryStringInput;
        private System.Windows.Forms.RichTextBox rtbTuringBinaryStringOutput;
    }
}

