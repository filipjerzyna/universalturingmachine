﻿using GalaSoft.MvvmLight.CommandWpf;
using PropertyChanged;
using System;
using System.Numerics;
using System.Windows;
using System.Windows.Input;
using UTM.Extensions;
using UTM.TuringMachine;

namespace UTM.ViewModels
{
    [ImplementPropertyChanged]
    internal class MainWindowViewModel
    {
        #region Properties

        public int OperationsLimit { get; set; }

        public string MachineNumber { get; set; }

        public string Program { get; set; }

        public string InputBinary { get; set; }

        public string OutputBinary { get; set; }

        public string InputDecimal { get; set; }

        public string OutputDecimal { get; set; }

        public Machine Machine { get; set; }

        #endregion Properties

        #region Commands

        public ICommand LoadCommand { get; set; }

        public ICommand ProcessCommand { get; set; }

        public ICommand MachineNumberFromProgramCommand { get; set; }

        public ICommand MachineNumberToProgramCommand { get; set; }

        #endregion Commands

        public MainWindowViewModel()
        {
            InitCommands();
            Program = String.Empty;
            InputDecimal = String.Empty;
            OperationsLimit = 10000;
        }

        private void InitCommands()
        {
            LoadCommand = new RelayCommand(Load, () => !String.IsNullOrWhiteSpace(Program));
            ProcessCommand = new RelayCommand(Process, () => Machine != null);
            MachineNumberFromProgramCommand = new RelayCommand(MachineNumberFromProgram, () => !String.IsNullOrWhiteSpace(Program));
            MachineNumberToProgramCommand = new RelayCommand(MachineNumberToProgram, () => !String.IsNullOrWhiteSpace(MachineNumber));
        }

        public void Load()
        {
            try
            {
                Machine = new Machine();
                Machine.LoadProgram(Program);
                MessageBox.Show("Program successfully loaded");
            }
            catch { }
        }

        public void Process()
        {
            try
            {
                var inputNumber = BigInteger.Parse(InputDecimal);
                var input = Converter.ToTuringBinaryString(inputNumber);
                InputBinary = input;

                var resultString = Machine.ProcessProgram(input, OperationsLimit);
                OutputBinary = resultString;
                var result = Converter.ToBigInteger(resultString);
                OutputDecimal = result.ToString();
            }
            catch { }
        }

        public void MachineNumberFromProgram()
        {
            var number = Program.ToDecimalBigInteger();
            MachineNumber = number.ToString();
        }

        public void MachineNumberToProgram()
        {
            var number = BigInteger.Parse(MachineNumber);
            var program = number.ToBinaryString();
            Program = program.TrimStart('0');
        }
    }
}