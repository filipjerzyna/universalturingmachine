﻿using System.Numerics;
using UTM.Extensions;

namespace UTM.TuringMachine
{
    internal static class Converter
    {
        private const string EndPart = "110";
        private const string Replacement = "10";

        public static string ToTuringBinaryString(BigInteger number)
        {
            var binaryString = number.ToBinaryString();
            binaryString = binaryString.Replace("1", Replacement);
            return binaryString + EndPart;
        }

        public static BigInteger ToBigInteger(string turingBinaryInput)
        {
            var withoutTrailingZeros = turingBinaryInput.TrimEnd('0');
            var inputWithoutEndPart = withoutTrailingZeros.Substring(0, withoutTrailingZeros.Length - 2);
            var binaryString = inputWithoutEndPart.Replace(Replacement, "1");

            var number = binaryString.ToDecimalBigInteger();
            return number;
        }
    }
}