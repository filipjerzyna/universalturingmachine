﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UTM.Models;

namespace UTM.TuringMachine
{
    internal class Machine
    {
        private static readonly Dictionary<string, string> Substitutions = new Dictionary<string, string>();
        private static readonly char[] Operations = new char[] { 'R', 'L', 'S' };

        private string stateBinary;

        /// <summary>
        /// Index of the instruction is the decimal form of the left side of Turing machine transition (state nr in binary form concatenated with symbol).
        /// </summary>
        private readonly List<Instruction> instructions;

        static Machine()
        {
            Substitutions.Add("0", "0");
            Substitutions.Add("10", "1");
            Substitutions.Add("110", "R");
            Substitutions.Add("1110", "L");
            //S instead of Stop for further convenience
            Substitutions.Add("11110", "S");
        }

        public Machine()
        {
            instructions = new List<Instruction>();
        }

        public void LoadProgram(string program)
        {
            instructions.Clear();

            //converting the binary form to a sequence of 0, 1, R, L and S chars
            var readableProgram = ConvertToReadableForm(program);

            //building the machine
            var instructionString = new StringBuilder();
            foreach (var symbol in readableProgram)
            {
                if (Operations.Contains(symbol))
                {
                    //zeros were omitted because of optimalization - we have to add them back
                    switch (instructionString.Length)
                    {
                        case 0:
                            instructionString.Append("00");
                            break;

                        case 1:
                            instructionString.Insert(0, '0');
                            break;
                    }
                    AddInstruction(instructionString.ToString(), symbol);
                    instructionString.Clear();
                }
                else
                {
                    instructionString.Append(symbol);
                }
            }
        }

        private static string ConvertToReadableForm(string program)
        {
            var readableProgram = new StringBuilder();
            var instructionString = new StringBuilder();

            readableProgram.Append('R');

            //translating to readable form
            foreach (var symbol in program)
            {
                instructionString.Append(symbol);

                string substitution;
                if (!Substitutions.TryGetValue(instructionString.ToString(), out substitution))
                    continue;
                readableProgram.Append(substitution);
                instructionString.Clear();
            }

            readableProgram.Append('R');

            return readableProgram.ToString();
        }

        private void AddInstruction(string instructionString, char operation)
        {
            var symbolToWrite = instructionString.Last();
            var sBinary = instructionString.Substring(0, instructionString.Length - 1);
            instructions.Add(new Instruction(sBinary, symbolToWrite, operation));
        }

        public string ProcessProgram(string input, int operationsLimit)
        {
            int debugCounter = 0;

            var inputArray = new InputArray(input);

            stateBinary = "0";

            var index = 0;
            var stop = false;

            while (!stop)
            {
                if (debugCounter++ == operationsLimit)
                    break;
                var key = BuildKey(stateBinary, inputArray[index]);
                var instruction = instructions[key];
                stateBinary = instruction.TargetStateBinary;
                inputArray[index] = instruction.SymbolToWrite;
                switch (instruction.Operation)
                {
                    case 'R':
                        index++;
                        break;

                    case 'L':
                        index--;
                        break;

                    case 'S':
                        stop = true;
                        break;
                }
            }

            return inputArray.ToString().Trim('0') + '0';
        }

        private static int BuildKey(string sBinary, char symbol)
        {
            var key = Convert.ToInt32(sBinary + symbol, 2);
            return key;
        }
    }
}