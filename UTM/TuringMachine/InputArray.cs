﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UTM.TuringMachine
{
    internal class InputArray
    {
        private readonly List<char> input;
        private int indexCorrection;

        public InputArray(string input)
        {
            this.input = input.ToList();
        }

        public char this[int index]
        {
            get
            {
                FixIndex(index + indexCorrection);
                return input[index + indexCorrection];
            }
            set
            {
                FixIndex(index + indexCorrection);
                input[index + indexCorrection] = value;
            }
        }

        private void FixIndex(int index)
        {
            if (index > input.Count - 1)
            {
                input.AddRange(Enumerable.Repeat('0', index - input.Count + 1));
            }
            else if (index < 0)
            {
                input.InsertRange(0, Enumerable.Repeat('0', Math.Abs(index)));
                indexCorrection += Math.Abs(index);
            }
        }

        public override string ToString()
        {
            return new String(input.ToArray());
        }
    }
}