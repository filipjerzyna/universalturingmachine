﻿namespace UTM.Models
{
    internal struct Instruction
    {
        public string TargetStateBinary;

        public char SymbolToWrite;

        public char Operation;

        public Instruction(string targetStateBinary, char symbolToWrite, char operation)
        {
            TargetStateBinary = targetStateBinary;
            SymbolToWrite = symbolToWrite;
            Operation = operation;
        }
    }
}